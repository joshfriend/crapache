/* ------------------------------------------

 CIS457 - Data Communications
 Project 2 - Crapache
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

 @author: Josh Friend, Chris Carr, Kyle Peltier, Aaron Herndon
------------------------------------------ */

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {

    private static final DateFormat format = new SimpleDateFormat("MM/dd/YY hh:mm:ss");
    private static final String lineSep = System.getProperty("line.separator");

    /**
     * A Custom format implementation that is designed for brevity.
     * Format: Crapache[SEVERE|Thread-1|02/18/13 7:28:09]: Unable to send response:
     * SEVERE Crapache.Thread-1.method()
     */
    public String format(LogRecord record) {
        String loggerName = record.getLoggerName();
        if(loggerName == null) {
            loggerName = "root";
        }
        StringBuilder output = new StringBuilder()
            .append(format.format(new Date(record.getMillis())) + " ")
            .append(String.format("%1$-" + 9 + "s", record.getLevel() + ": "))
            .append(loggerName + ".")
            .append(Thread.currentThread().getName() + ".")
            .append(record.getSourceMethodName() + "(): ")
            .append(record.getMessage().replaceAll("\n", "\n\t"))
            .append(lineSep);
        return output.toString();
    }
}