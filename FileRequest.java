/**
 * FileRequest class
 * TO-DO:
 *  get info for headers
 *  CONTENT-LENGTH: get file's length
 *  CONTENT-TYPE: html, txt, jpg, pdf
 *  ERROR-CHECKING: Return message if file does not exist
 *  LAST-MODIFIED header: send back the date/time
 *      Example -- Last-Modified: Tue, 15 Nov 1994 12:45:26 GMT
 *
 *  NOTES: In my concept of passing in a FileRequest object in to a method
 *  in the CLientHandler class, I'm not sure how to handle getting
 *  the messages back. Briefly looking over JNetPcap, it appears the
 *  hasHeader() method was overloaded (or using some kind of Interface or
 *  Polymorphism) to get the various bits of information by passing in
 *  Ethernet and Ip4 objects.
 *  BTW - I am totally fine with doing it some other way (that's easier!)
 *  @author: Josh Friend, Chris Carr, Kyle Peltier, Aaron Herndon
 */

import java.nio.file.*;
import java.nio.file.attribute.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.net.URLConnection;

public class FileRequest {
    private Path docroot;   // root path directory
    private Path location;  // unioned docroot and given file

    public static DateFormat dateFormatter = new SimpleDateFormat("EEE, MMM dd HH:mm:ss yyyy");

    /* CTOR: args: two Strings that when concatenated = path/to/file
     * arg1: docroot from Server class, arg2: file name requested
     */
    public FileRequest(String docroot, String location) {
        try {
            // Set properties of this file
            this.docroot = Paths.get(docroot);
            this.location = Paths.get(docroot, location);
        }
        catch(Exception e) {
            // Nothing to do here...
        }
    }

    /**
     * Determine if path given is a directory
     * @return true if directory, false if error (not a directory).
     */
    public boolean isDirectory(){
        try {
            return Files.isDirectory(location, java.nio.file.LinkOption.NOFOLLOW_LINKS);
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Determine if the directory has an index file
     * @return true: has index. false: does not have index.
     */
    public boolean hasIndex() {
        // return false;
        File[] files = new File(location.toString()).listFiles();
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:*{index,home}.{htm,html,txt}");

        for(File f : files) {
            // System.out.println(f);
            if (matcher.matches(Paths.get(f.toString()))) {
                return true;
            }
        }
        return false;
    }

    /**
     * UNIMPLEMENTED.
     * Return the index, if one is found.
     * @return
     */
    public FileRequest getIndex() {
        return null;
    }

    /**
     * Determine if the path given to CTOR exists
     * @return true: path exists. false: path does not exist.
     */
    public boolean exists() {
        try {
            return Files.exists(location, java.nio.file.LinkOption.NOFOLLOW_LINKS);
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Determine if the user has permission to access the path given to CTOR.
     * @return @true: Has permission.
     *         @false: Does not have permission
     */
    public boolean havePermission() {
        try {
            return ((Files.isReadable(location)) && (location.toAbsolutePath().compareTo(docroot) >= 0));
        }
        catch (Exception e) {
            return false;
        }
    }

    /**
     * Determine the length of the file.
     * @return @Type <long>: file size in bytes.
     *         @IOException: 0
     */
    private long getFileLength() {
        try {
            return Files.size(location);
        }
        catch (IOException e) {
            return 0;
        }
    }

    /**
     * Determine the last modified time of the file.
     * @return @Type <Date>
     */
    public Date lastModified() {
        try {
            FileTime fileModTime = Files.getLastModifiedTime(location, java.nio.file.LinkOption.NOFOLLOW_LINKS);
            Date modDate = new Date(fileModTime.toMillis());
            return modDate;
        }
        catch (IOException e) {
            return null;
        }
    }

    /**
     * Create an InputStream to read bytes from the file.
     * @return @Type <InputStream>
     * @throws IOException
     */
    public InputStream getFileStream() throws IOException {
        return Files.newInputStream(location);
    }

    /**
     * Get the content MIME type of the file.
     * @return @Type <String>: Content MEME.
     */
    private String getContentType() {
        return URLConnection.guessContentTypeFromName(location.toString());
    }

    /**
     * Compare the last modified date to the passed date.
     * Parse date of format: Wed, 19 Oct 2005 10:50:00 GMT
     * @param checkDate
     * @return @true: Has been modified since.
     *         @false: Has not been modified since
     */
    public boolean isModifiedSince(String checkDate) {
        Date check = new Date();
        try{
            check = dateFormatter.parse(checkDate);
        }
        catch(Exception e) {
            // Have to assume it was modified because we cant really tell
            return true;
        }

        return check.compareTo(lastModified()) > 0;
    }

    /**
     * Get HTTP header options/info
     * @return @Type <HashMap <String,String>>: HTTP header information.
     */
    public HashMap<String,String> getHeaderOptions(){
        // Get HTTP header options/info
        HashMap<String,String> options = new HashMap<String,String>();
        options.put("Last-Modified", dateFormatter.format(lastModified()));
        options.put("Content-Length", "" + getFileLength());
        options.put("Content-Type", getContentType());
        return options;
    }
}



