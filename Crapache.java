/** ------------------------------------------

 CIS457 - Data Communications
 Project 2 - Crapache
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

@author: Josh Friend, Chris Carr, Kyle Peltier, Aaron Herndon
------------------------------------------ */

import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.*;
import java.util.concurrent.*;
import java.nio.file.*;


public class Crapache {
    private final static Logger log = Logger.getLogger(Crapache.class.getName());
    private ServerSocket listenSocket;

    public File docroot;

    /**
     * Create a new instance of Crapache Server
     * @param port
     * @param docroot
     */
    public Crapache(int port, File docroot) {
        this.docroot = docroot;

        try {
            listenSocket = new ServerSocket(port);
            log.info("Started server, listening on port " + port);
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Server could not start on port " + port, e);
            System.exit(-1);
        }

        listen();
    }

    /**
     * Listens forever or until terminated.
     * Listening for new connection requests.
     */
    public void listen() {
        // Listens forever (or until terminated)
        while(!listenSocket.isClosed()) {
            try {
                Socket s = listenSocket.accept();
                log.info("Accepted new connection from: " + s.getRemoteSocketAddress());
                Worker w = new Worker(s, docroot);
                w.start();
                log.info("Dispatched new worker to handle request.");
            }
            // ServerSocket.accept() throws SocketException when closing the TCP Socket
            catch(SocketException e) {
                log.warning("Server shutting down... ");
                System.exit(0);
            }
            catch(IOException e) {
                log.log(Level.SEVERE, "Error accepting new connection!", e);
                System.exit(-1);
            }
        }
    }

    /**
     * Safely close open socket sessions.
     */
    public void close() {
        try {
            listenSocket.close();
            log.info("Server port " + listenSocket.getLocalPort() + " closed...");
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Unable to close server socket!", e);
        }
    }

    /**
     * Configure the logging style of the project (Specified in logging param)
     */
    private void configureLogging() {
        // Remove defaul log handlers
        Handler[] handlers = log.getHandlers();
        for(Handler handler : handlers) {
            log.removeHandler(handler);
        }

        // Add custom handlers
        Handler ch = new ConsoleHandler();
        ch.setFormatter(new LogFormatter());
        log.addHandler(ch);
    }

    /**
     * Parses the command line arguments into a hashmap of strings.
     * @param args
     * @return @Type <HashMap<String, String>>: Hashmap of arguments
     */
    public static HashMap<String, String> mapArgs(String args[]) {
        // A simple commandline argument parser
        HashMap<String, String> argMap = new HashMap<String, String>();
        String flag = "";

        // Iterate through args, skipping first (which is the program name)
        for(int i = 0; i < args.length; i++){
            if(args[i].startsWith("-")) {
                if(!flag.equals("")) {
                    // last flag was argumentless flag
                    argMap.put(flag, "");

                    // Set flag
                    flag = args[i].substring(1);
                }
                else {
                    // Is a new flag
                    flag = args[i].substring(1).trim();
                }

            }
            else {
                if(flag.equals("")) {
                    // Does not support positional arguments or multiple args per flag
                    log.warning("Ignoring positional argument: '" + args[i] + "'");
                }
                else {
                    // Store value of flag
                    argMap.put(flag, args[i].trim());

                    // Reset flag
                    flag = "";
                }
            }
        }

        // Handle trailing argless flags
        if(!flag.equals("")) {
            argMap.put(flag, "");
        }

        return argMap;
    }

    /**
     * Main of Crapache. Begins a new Crapache Session.
     * @param args Takes in an array of
     * arguments regarding port, log file, and level of logging.
     * Parse Verbosity Options:
     * SEVERE (qq)
     * WARNING (q)
     * INFO
     * CONFIG
     * FINE (v)
     * FINER (vv)
     * FINEST (lowest value, vvv)
     *
     */
    public static void main(String args[]) {
        // Remove defaul log handlers
        Logger globalLogger = Logger.getLogger("");
        Handler[] handlers = globalLogger.getHandlers();
        for(Handler handler : handlers) {
            globalLogger.removeHandler(handler);
        }

        // Add custom handlers
        Handler ch = new ConsoleHandler();
        ch.setFormatter(new LogFormatter());
        globalLogger.addHandler(ch);

        // Log level show info or higher messages
        globalLogger.setLevel(Level.INFO);

        // Default options
        int port = 8080;
        Path docroot = null;
        File logfile = null;

        // Parse commandline args into a map
        HashMap<String, String> argMap = mapArgs(args);

        // Parse verbosity options
        if(argMap.containsKey("v")) {
            globalLogger.setLevel(Level.FINE);
        }
        else if(argMap.containsKey("vv")) {
            globalLogger.setLevel(Level.FINER);
        }
        else if(argMap.containsKey("vvv")) {
            globalLogger.setLevel(Level.FINEST);
        }
        else if(argMap.containsKey("q")) {
            globalLogger.setLevel(Level.WARNING);
        }
        else if(argMap.containsKey("qq")) {
            globalLogger.setLevel(Level.SEVERE);
        }
        else if(argMap.containsKey("qqq")) {
            globalLogger.setLevel(Level.OFF);
        }
        // Apply leve, to all loggers
        for (Handler h : globalLogger.getHandlers()) {
            h.setLevel(globalLogger.getLevel());
        }

        // Help menu
        if(argMap.containsKey("h") || argMap.containsKey("help")) {
            System.out.println("Commandline Flags:");
            System.out.println("-docroot    Directory to serve webpages from");
            System.out.println("-logfile    File to store log messages. If not specified messages only print to console");
            System.out.println("-p          Port number to listen on\n");

            System.out.println("Logging verbosity flags (Level defaults to INFO)");
            System.out.println("-v          Show only FINE level messages or above");
            System.out.println("-vv         Show only FINER level messages or above");
            System.out.println("-vvv        Show ALL levels of log messages");
            System.out.println("-q          Show only WARNING level messages or above");
            System.out.println("-qq         Show only SEVERE level messages");
            System.out.println("-qqq        All log messages OFF");
            System.exit(0);
        }

        // Parse log directory
        if(argMap.containsKey("logfile")) {
            // check if file exists
            logfile = new File(argMap.get("logfile"));
            if(!logfile.exists()){
                log.warning("Logfile does not exist, it will be created...");
                try{
                    PrintWriter file = new PrintWriter(logfile, "UTF-8");
                }
                catch(IOException ie){
                    log.info("Unable to create file " + logfile);
                }
            }
            else {
                log.info("Server log messages will be stored to " + logfile);
                // Set up file handler for loggers
            }
            try {
                FileHandler fh = new FileHandler(logfile.toString());
                LogFormatter sf = new LogFormatter();
                fh.setFormatter(sf);
                // Add handler to global logger so future logger instances will inherit it
                Logger.getLogger("").addHandler(fh);
            }
            catch (IOException e) {
                log.log(Level.SEVERE, "Error opening log file " + logfile + "for writing: " + e);
            }
        }
        else {
            logfile = null;
            log.info("-logfile not specified, will only log to console");
        }

        // Parse port number
        if(argMap.containsKey("p")) {
            try {
                port = Integer.parseInt(argMap.get("p"));
            }
            catch (NumberFormatException e) {
                log.severe("Invalid port number: " + argMap);
            }
        }

        // Parse docroot
        /* DocumentRoot is a directory which contains index.html and other web site files */
        if(argMap.containsKey("docroot")) {
            // check if file exists
            docroot = Paths.get(argMap.get("docroot"));

            if(Files.notExists(docroot)) {
                log.severe("docroot does not exist!");
                System.exit(-1);
            }
            else if (! Files.isDirectory(docroot, java.nio.file.LinkOption.NOFOLLOW_LINKS)) {
                log.severe("docroot is not a directory!");
                System.exit(-1);
            }
        }
        else {
            /* Document Root should be Current Working Directory if none is specified */
            docroot = Paths.get(".");
        }

        // Show logging level
        log.info("Logging level set to " + globalLogger.getLevel().getName() + "...");

        // Initialize server object
        log.info("Startup Directory: " + docroot);
        Crapache cs = new Crapache(port, new File(docroot.toString()));
    }
}
