/* ------------------------------------------

 CIS457 - Data Communications
 Project 1 - TCP Chat Client/Server
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle
@author: Josh Friend, Chris Carr, Kyle Peltier, Aaron Herndon
------------------------------------------ */

import java.io.*;
import java.net.*;
import java.util.logging.*;

public class Worker extends Thread {

    private final static Logger log = Logger.getLogger(Worker.class.getName());
    private Socket socket;
    private InputStreamReader inputStream;
    private DataOutputStream outputStream;
    private File docroot;
    private boolean socketClosed;

    /**
     * Create a new worker for the incoming request on socket, searching for
     * docroot.
     * @param socket Client socket opened
     * @param docroot Path requested
     */
    public Worker(Socket socket, File docroot) {
        this.socket = socket;
        this.docroot = docroot;
        socketClosed = false;

        // Check for null docroot
        if(this.docroot == null) {
            log.info("DOCROOT IS NULL");
        }

        // 20 second timeout
        try {
            this.socket.setSoTimeout(5*1000);
        }
        catch (SocketException e) {
            log.log(Level.SEVERE, "Error setting socket timeout:", e);
        }

        // Attach stream reader/writer to socket
        try {
            inputStream = new InputStreamReader(socket.getInputStream());
            outputStream = new DataOutputStream(socket.getOutputStream());
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Unable to get input stream from socket:", e);
        }

        log.info("Created worker thread!");
    }

    /**
     * Run the new worker thread. Log everything if specified in starting args.
     */
    public void run() {
        log.info("Worker thread running!");
        String reqstr = null;
        HTTPRequest request;

        try {
            while(true) {
                try {
                    // Parse HTTP request
                    request = new HTTPRequest(getRequest());
                    if(request.method != null) {
                        log.info("Request:\n" + request);
                        handleRequest(request);
                    }
                }
                catch (SocketTimeoutException e) {
                    log.warning("Socket timeout!");
                    // Break loop and close connection
                    break;
                }
                catch (SocketException e) {
                    log.warning("Socket closed!");
                    break;
                }
                catch (IOException e) {
                    if(!socketClosed) {
                        log.log(Level.SEVERE, "Unable to get request: ", e);
                        // 500 Internal Server Error
                        log.warning("500 Internal Server Error!");
                    }
                    break;
                    // HTTPResponse response = new HTTPResponse(500);
                    // // Set file target to relevant error image
                    // FileRequest filerequest = new FileRequest(docroot.toString(), "images/" + response.status + ".jpg");
                    // // Send content type, length, modified, etc...
                    // response.putAll(filerequest.getHeaderOptions());
                    // sendResponse(response);
                    // // Send payload
                    // sendPayload(filerequest.getFileStream());
                }
            }
        }
        catch (Exception e) {
            log.log(Level.SEVERE, "Error: ", e);
            e.printStackTrace();
        }

        // Nothing left to do for now
        close();
        log.info("Worker thread finished!");
    }

    /**
     * Get the request/read in from the socket
     * @return String request
     * @throws IOException
     */
    public String getRequest() throws IOException {
        String request = "";
        char[] buffer = new char[4096];
        int numRead;

        // Double CRLF marks end of request
        while(!request.endsWith("\r\n\r\n")) {
            // Check stream first to save a TON of CPU cycles
            // if(inputStream.ready()) {
            // String line = inputStream.readLine();
            log.finer("Reading form socket...");
            numRead = inputStream.read(buffer, 0, buffer.length);
            log.finer("Read from socket!");
            if(numRead != -1) {
                request += new String(buffer).substring(0, numRead);
            }
            else {
                try {
                    // Sleep so we don't PWN the CPU constantly...
                    Thread.sleep(10);
                }
                catch (InterruptedException e) {
                    log.log(Level.WARNING, "Sleep error!", e);
                }
            }
        }
        return request;
    }

    /**
     * Send the response over the open socket back to the client.
     * @param HTTPResponse
     */
    private void sendResponse(HTTPResponse response) {

        try {
            String header = response.toString();
            log.info("Response:\n" + header.substring(0, header.length() - 4) + "\n<paylod data>\n");
            outputStream.writeBytes(header);
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Unable to send response: ", e);
            close();
        }
    }

    /**
     * Handle the request from the client/
     * Check for file existence.
     * Check that the method is GET
     * Call helper methods to find the file
     * Detect keepalive
     * @param req
     */
    private void handleRequest(HTTPRequest req) {
        HTTPResponse response = new HTTPResponse(200);
        FileRequest filerequest = null;
        boolean skip = false;
        boolean keepalive = true;
        // We only support HTTP GET method
        if(req.method.equals("GET")) {
            // Create new FileRequest object to handle file IO
            filerequest = new FileRequest(docroot.toString(), req.location);

            // Check if file exists
            if(!filerequest.exists()) {
                // 404 Not Found
                log.warning("404 Not Found!");
                response = new HTTPResponse(404);
                filerequest = new FileRequest(docroot.toString(), "images/" + response.status + ".jpg");
                skip = true;
            }

            // If request is a directory, attempt to get the index page
            if(filerequest.isDirectory() && !skip) {
                log.info("Requsted directory, attempting to get index");
                // Look for index.* type file to display
                if(filerequest.hasIndex()) {
                    log.info("Index found!");
                    // Get index page
                    filerequest = filerequest.getIndex();
                }
                else {
                    // 404 Not Found
                    log.warning("Index could not be located");
                    response = new HTTPResponse(404);
                    filerequest = new FileRequest(docroot.toString(), "images/" + response.status + ".jpg");
                    skip = true;
                }
            }

            // Check if user has permission to access the file
            if(!filerequest.havePermission() && !skip) {
                // 403 Forbidden
                log.warning("403 Forbidden!");
                response = new HTTPResponse(403);
                skip = true;
            }

            // Check if server requested modification check
            if(req.get("If-Modified-Since") != null && !skip) {
                // Check if file was modified since time given by server
                if(filerequest.isModifiedSince(req.get("Is-Modified-Since"))) {
                    // File has not changed, send status 304
                    log.warning("304 Not Changed...");
                    response = new HTTPResponse(304);
                }
                else {
                    log.info("Page data outdated, sending update...");
                }
            }
            else {
                log.info("Not checking modify time...");
            }
        }
        // Methods other than GET not implemented
        else {
            // 501 Not Implemented
            log.warning("501 Not Implemented!");
            response = new HTTPResponse(501);
        }

        // OK now we send the actuall stuff...
        try {
            // Check for keepalive request
            if(req.get("Connection").equals("keep-alive")) {
                // Sender requested keepalive
                response.put("Connection", "keep-alive");
            }
            if(req.get("Connection") == null) {
                // We are forcing connection to close because keepalive was not requested
                response.put("Connection", "close");
            }
            if(response.get("Connection").equals("close")) {
                // Parse flag
                keepalive = false;
            }

            // Handle error responses
            if(response.status >= 400) {
                // Set file target to relevant error image
                filerequest = new FileRequest(docroot.toString(), "images/" + response.status + ".jpg");
                // Send content type, length, modified, etc...
                response.putAll(filerequest.getHeaderOptions());
                sendResponse(response);
                // Send payload
                sendPayload(filerequest.getFileStream());
            }
            else if(response.status == 200) {
                // Send content type, length, modified, etc...
                response.putAll(filerequest.getHeaderOptions());
                sendResponse(response);

                // Send payload
                sendPayload(filerequest.getFileStream());
            }
            else {
                // 304 Not Modified
                // Send content type, length, modified, etc...
                response.putAll(filerequest.getHeaderOptions());
                response.put("Content-Length", "0");
                sendResponse(response);
            }
        }
        catch (Exception e) {
            // 500 Internal server error
            log.log(Level.SEVERE, "500 Internal Server Error!", e);
            e.printStackTrace();
            response = new HTTPResponse(500);
            sendResponse(response);
        }

        if(!keepalive) {
            // Keepalive was not requested
            log.info("Keepalive not requested, closing socket...");
            close();
        }

        // close();
    }

    /**
     * Read the file requested into the stream between the client and server.
     * @param file
     * @throws IOException
     */
    private void sendPayload(InputStream file) throws IOException {
        byte[] buffer = new byte[4096];
        while(true) {
            int numRead = file.read(buffer, 0, buffer.length);
            if(numRead == -1) {
                break;
            }
            outputStream.write(buffer, 0, numRead);
        }
        log.info("Transmitted payload...");
    }

    /**
     * Close the socket safely
     */
    public void close() {
        // Close input stream
        if(socketClosed) {
            // If already closed
            return;
        }
        try {
            inputStream.close();
            log.info("Input Stream closed!");
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Error closing input stream:", e);
        }
        catch (NullPointerException e) {
            log.log(Level.SEVERE, "Error closing input stream:", e);
        }

        // Close socket
        try {
            socket.close();
            log.info("Socket closed!");
        }
        catch (IOException e) {
            log.log(Level.SEVERE, "Error closing socket:", e);
        }
        catch (NullPointerException e) {
            log.log(Level.INFO, "Socket appears to be already closed:", e);
        }
        // Mark flag to indicate we willfully closed socket
        socketClosed = true;
    }
}
