/* ------------------------------------------

 CIS457 - Data Communications
 Project 2 - Crapache
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

@author: Josh Friend, Chris Carr, Kyle Peltier, Aaron Herndon
------------------------------------------ */

import java.io.*;
import java.util.*;
import java.text.*;
import java.util.regex.*;


class HTTPResponse {

    private HashMap<String, String> options;
    public static final String version = "HTTP/1.x";
    public static DateFormat dateFormatter = new SimpleDateFormat("EEE, MMM dd HH:mm:ss yyyy");
    public String statusMessage;
    public int status;

    /**
     * HTTPResponse constructor for no parameter input.
     * Default: Status 501, Not Implemented.
     */
    public HTTPResponse() {
        // Hardcoded for now...
        status = 501;
        statusMessage = "Not Implemented";

        options = new HashMap<String, String>();
        options.put("Date", "Sat, 28 Nov 2009 04:36:25 GMT");
        options.put("Server", "Crapache");
        options.put("Connection", "close");
    }

    /**
     * HTTPResponse constructor for specified status code.
     * @param code defines page return status.
     */
    public HTTPResponse(int code) {
        options = new HashMap<String, String>();

        // Put date in header
        Date currentDate = new Date();
        HashMap<String,String> options = new HashMap<String,String>();
        put("Date", dateFormatter.format(currentDate));
        put("Server", "Crapache");

        status = code;
        switch(status) {
            case 200:
                statusMessage = "OK";
                break;
            case 304:
                statusMessage = "Not Modified";
                break;
            case 401:
                statusMessage = "Unauthorized";
                put("Connection", "close");
                break;
            case 403:
                statusMessage = "Forbidden";
                put("Connection", "close");
                break;
            case 404:
                statusMessage = "Not Found";
                put("Connection", "close");
                break;
            case 418:
                statusMessage = "I'm A Teapot";
                put("Connection", "close");
                break;
            case 500:
                statusMessage = "Internal Server Error";
                put("Connection", "close");
                break;
            case 501:
                statusMessage = "Unimplemented";
                put("Connection", "close");
                break;
            default:
                break;
        }
    }

    /**
     * Put into options
     * @param key
     * @param value
     */
    public void put(String key, String value) {
        options.put(key, value);
    }

    /**
     * Put a Hashmap into options
     * @param opts
     */
    public void putAll(HashMap<String, String> opts) {
        options.putAll(opts);
    }

    /**
     * Get the content of key in the options hashmap.
     * @param key
     * @return
     */
    public String get(String key) {
        return options.get(key);
    }

    /**
     * Map of options to string.
     * @return String
     */
    public String toString() {
        String s = version + " " + status + " " + statusMessage + "\r\n";
        for(Map.Entry<String, String> e : options.entrySet()) {
            s += e.getKey() + ": " + e.getValue() + "\r\n";
        }
        return s + "\r\n";
    }
}