/* ------------------------------------------

 CIS457 - Data Communications
 Project 2 - Crapache
 GVSU 2013

              **STARRING**
    __     ______     ______     __  __
   /\ \   /\  __ \   /\  ___\   /\ \/ /
  _\_\ \  \ \  __ \  \ \ \____  \ \  _"-.
 /\_____\  \ \_\ \_\  \ \_____\  \ \_\ \_\
 \/_____/   \/_/\/_/   \/_____/   \/_/\/_/
   Josh       Aaron      Chris      Kyle

@author: Josh Friend, Chris Carr, Kyle Peltier, Aaron
------------------------------------------ */

import java.io.*;
import java.util.*;
import java.util.regex.*;


class HTTPRequest {
    private HashMap<String, String> options;
    private static final String method_regex = "(GET|OPTIONS|HEAD|POST|PUT|DELETE|TRACE|CONNECT)\\s+(\\S+)\\s+(\\S+)";
    private static final String options_regex = "(.*): (.*)\r\n";

    public String method;       // HTTP Method
    public String location;     // Location argument
    public String version;      // HTTP version of request

    /**
     * Creates a new HTTP request
     * @param: String
     */
    public HTTPRequest(String raw) {
        // Parse HTTP method
        Matcher m = Pattern.compile(method_regex).matcher(raw);
        // method = m.group(1);
        if(m.find()) {
            method = m.group(1);
            location = m.group(2);
            version = m.group(3);
        }

        // Parse options
        m = Pattern.compile(options_regex).matcher(raw);
        options = new HashMap<String, String>();
        while(m.find()) {
            options.put(m.group(1), m.group(2));
        }
    }

    /**
     * HTTP Request to String
     * @return String
     */
    public String toString() {
        if(method == null) {
            return null;
        }
        String s = method + " " + location + " " + version + "\r\n";
        for(Map.Entry<String, String> e : options.entrySet()) {
            s += e.getKey() + ": " + e.getValue() + "\n";
        }
        return s;
    }

    /**
     * Returns the HTTPRequest key
     * @param: String
     * @return: String
     */
    public String get(String key) {
        return options.get(key);
    }
}