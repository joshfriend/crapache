# CIS 457 Project 2: Mini HTTP Server
* __Objective__: Implement a (subset of a) real protocol.
* __Deliverables__: You must turn in your code on blackboard by 11:59 PM on 2/25/2013. Additionally you must demo your client/server on 2/26/2013 in lab. If you do not demo, you will recieve no credit (all group members should be present). Your group evaluation forms (available on blackboard) are due in hard copy by 2/27/2013.

This project should be done in groups of 4. If you are deviating from the groups on blackboard, please send an email to the instructor as soon as possible detailing the new group membership. Only one person per group needs to send this message.

## Authors
* Aaron Herndon
* Chris Carr
* Josh Friend
* Kyle Peltier

## Introduction
In this project, we will be implementing a web (HTTP/1.1) server. HTTP, defined in [RFC 2616](http://www.ietf.org/rfc/rfc2616.txt) is a large and complex protocol. Therefore, you do not need to implement the entire HTTP/1.1 specification in the RFC. You must implement the subset of the protocol described below. Your server must be able to interact with a standard web browser (a recent version of firefox). Wireshark must be able to interperit the messages your server sends. HTTP will be explained in lab. For more details, find a good reference online, or ask specific questions on the message board.

You must write this program in Java, C, or C++. You may not use any non-standard libraries without prior permission. You also may not use any libraries which automatically parse HTTP requests. You only need to write the server portion of this application, not the client. Your server should run on the datacomm lab computers, preferably on Windows Server 2008.

## Invocation
Your server should accept the following command line arguments:

* __-p__ *<port\>*: Specify the port the server should run on. If no port is specified, the default should be 8080.
* __-docroot__ *<directory\>*: Specify the directory from which the server should look for requested files. The default should be the directory the server was run from.
* __-logfile__ *<file\>*: Specify a file for log messages to be written out to. If no log file is specified, then no log messages should be writted.

## Behavior:
Your server must do the following:

* Your server must support multiple simultaneous clients or multiple connections from the same client. You will likely need threading for this.
* You only need to support the GET operation, other operations such as POST do not need to be supported. You must return the appropriate status code (501) in response to such requests to indicate this.
* You must send the date header.
* You must send the last-modified header.
* You must send a content-type header, but you only need to support the types for html, plain text, jpeg images, and pdf files. You may of course support others.
* You must send a header indicating the length of the content you are sending.
* You must return page contents and a 200 status code if the client requests a vaild document.
* You must return a 404 status code if the client requests and invalid document. Note that a 404 response has a body: you must include the error page to be displayed on the client.
* You must correctly interperit any if-modified-since header sent by the client, including returning the appropriate status code (304) if the page has not been modified, and detecting if the page has been modified on disk.
* You must support persistent connections. A browser may send several requests over the same HTTP connection (even before receiving any responses). If a recieved message contains a connection: close header, the connection must be closed immediately after handling the request. Otherwise, the connection must be kept open for 20 seconds, and closed if no messages have been recieved after this amount of time.
* Unsupported headers in requests must not cause your server to fail.
* Your server must print all requests it recieves to the log file, along with the headers (only) of responses it sends. Printing to the log must be thread-safe: a message printed by one thread should not occur in the middle of a message from another.
* You must prevent your server from accessing files outside of its docroot directory, as this is a security risk.

## Grading:
There will be a total of 20 points, divided as follows:

    Code compiles without error                 1
    Command line options                        1
    Proper runtime error handling               2
    Proper handling of valid documents (200)    3
    Proper handling of errors (404, 501)        2
    Multiple simultaneous clients               2
    Persistent connections                      3
    If-modified-since                           2
    Log file                                    1
    Security                                    1
    Group evaluation                            2
